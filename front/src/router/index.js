import Vue from 'vue';
import VueRouter from 'vue-router';
import MovieList from '../views/MovieList.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'MovieList',
    component: MovieList,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
