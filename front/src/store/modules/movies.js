import axios from 'axios';

const MOVIES_API_URL = 'https://movies-api.alexgalinier.now.sh';

// Initial state
const state = {
  movies: [],
  moviesStatus: null,
};

// getters
const getters = {
  getAll: state => state.movies,
  getStatus: state => state.moviesStatus,
};

// actions
const actions = {
  async getMovies({ commit }) {
    commit('moviesStatus', null);
    try {
      const res = await axios.get(MOVIES_API_URL);
      commit('moviesStatus', 'success');
      commit('setMovies', res.data);
    } catch (e) {
      commit('moviesStatus', 'failed');
    }
  },
};

// mutations
const mutations = {
  moviesStatus(state, moviesStatus) {
    state.moviesStatus = moviesStatus;
  },
  setMovies(state, movies) {
    state.movies = movies;
  },
  addMovie(state, movie) {
    state.movies.push(movie);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
