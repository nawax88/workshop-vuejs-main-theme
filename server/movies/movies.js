const uuidV4 = require('uuid/v4');

module.exports = [
  {
    id: uuidV4(),
    name: 'Retour vers le futur',
    url: 'http://retourverslefutur.com',
    year: 1979,
  },
];
