const Koa = require('koa');
const Router = require('koa-router');
const body = require('koa-bodyparser');
const validUrl = require('valid-url');
const uuidV4 = require('uuid/v4');

// Split : https://medium.com/@bmikkelsen22/designing-a-serverless-express-js-api-using-zeit-now-6e52aa962235
// mongodb+srv://alex:2;a)&rWYFr@workshop-vuejs-5yfty.mongodb.net/test?retryWrites=true&w=majority

const app = new Koa();
const router = new Router();

app.use(body());
app.use(async (ctx, next) => {
  ctx.accepts('application/json');
  ctx.acceptsCharsets('utf-8');
  await next();
});
app.use(async (ctx, next) => {
  // TODO: Instead of a `*` we will take the value from `config`
  ctx.set('Access-Control-Allow-Origin', '*');
  ctx.set('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS');
  ctx.set(
    'Access-Control-Allow-Headers',
    'Content-Type, Content-length, Authorization',
  );
  await next();
});

let movies = require('./movies');

router.get('/', ctx => {
  ctx.body = movies;
});

const httpError = (ctx, message) => {
  ctx.body = message;
  ctx.status = 403;
};

router.post('/', (ctx, next) => {
  const res = ctx.request.body;

  // Validation
  if (movies.length >= 100)
    return httpError(ctx, 'We cannot have more than 100 movies');

  if (!res.name) return httpError(ctx, 'Name is missing');
  if (typeof res.name !== 'string')
    return httpError(ctx, 'Name must be a string');
  if (res.name.length > 40)
    return httpError(ctx, 'Name must be under 40 chars');

  if (!res.url) return httpError(ctx, 'Url is missing');
  if (typeof res.url !== 'string')
    return httpError(ctx, 'Url must be a string');
  if (!validUrl.isUri(res.url))
    return httpError(ctx, 'Url has not a valid URI format');

  if (!res.year) return httpError(ctx, 'Year is missing');
  if (typeof res.year !== 'number')
    return httpError(ctx, 'Year must be a number');
  if (res.year < 1900 || res.year > 2020)
    return httpError(ctx, 'Year must be between 1900 and 2020 (included)');

  const newMovie = {
    id: uuidV4(),
    name: res.name,
    url: res.url,
    year: res.year,
  };

  movies.push(newMovie);

  ctx.status = 201;
  ctx.body = newMovie;
});

app.use(router.routes()).use(router.allowedMethods());
app.listen(3000);
