const uuidV4 = require('uuid/v4');

module.exports = [
  {
    id: uuidV4(),
    url: 'https://wallpapercave.com/wp/IwnDnDi.jpg',
    name: 'Pulp Fiction',
    year: '1994',
  },
  {
    id: uuidV4(),
    url: 'https://wallpapercave.com/wp/wp3943026.jpg',
    name: "2001 : L'Odyssée de l'espace",
    year: '1968',
  },
  {
    id: uuidV4(),
    url: 'https://wallpapercave.com/wp/wp1820019.jpg',
    name: 'Blade Runner',
    year: '1982',
  },
  {
    id: uuidV4(),
    url: 'https://wallpapercave.com/wp/wp2330961.jpg',
    name: '12 Angry men',
    year: '1957',
  },
  {
    id: uuidV4(),
    url: 'https://wallpapercave.com/wp/wp1933268.jpg',
    name: 'Die Hard',
    year: '1988',
  },
  {
    id: uuidV4(),
    url: 'https://wallpapercave.com/wp/g6uLsor.jpg',
    name: 'Apocalypse Now',
    year: '1979',
  },
  {
    id: uuidV4(),
    url: 'https://wallpapercave.com/wp/wp2289076.jpg',
    name: 'Coco',
    year: '2017',
  },
  {
    id: uuidV4(),
    url: 'https://wallpapercave.com/wp/wp1836522.jpg',
    name: 'Vice et versa',
    year: '2013',
  },
  {
    id: uuidV4(),
    url: 'https://wallpapercave.com/wp/wp2294871.jpg',
    name: 'American History X',
    year: '1998',
  },
  {
    id: uuidV4(),
    url: 'https://wallpapercave.com/wp/wp2372753.jpg',
    name: 'Memento',
    year: '2000',
  },
  {
    id: uuidV4(),
    url: 'https://wallpapercave.com/wp/wp4138118.jpg',
    name: 'Rain man',
    year: '1988',
  },
  {
    id: uuidV4(),
    url: 'https://wallpapercave.com/wp/wp2147823.jpg',
    name: 'Usual suspect',
    year: '1995',
  },
  {
    id: uuidV4(),
    url: 'https://wallpapercave.com/wp/wp4119601.jpg',
    name: 'Le seigneur des anneaux',
    year: '2001',
  },
  {
    id: uuidV4(),
    url: 'https://wallpapercave.com/wp/wp4138533.png',
    name: 'The Truman show',
    year: '1998',
  },
];
